#include <err.h>
#include <errno.h>
#include <locale.h>
#include <fts.h>

/*
 * call 'stat' for each files through a file path hierarchy
 * see FTS(3)
 * 
 * zfsCheck.c
 */

int
main(int argc, char *argv[])
{

	FTS            *fts;
	FTSENT         *p;
	setlocale(LC_ALL, "");
	int		rval = 0;
	char           *const plist[] = {".", NULL};

	/*
	   * fts:
	   * No pattern, all files
	   * process symbolic links, not their target
	   * do not descend into other devices
	   * do not reorder
	   *
	   */
	if ((fts = fts_open(plist, FTS_PHYSICAL | FTS_XDEV, NULL)) == NULL)
		err(1, "fts_open");


	while ((p = fts_read(fts)) != NULL) {

		switch (p->fts_info) {
		case FTS_DNR:
			/*
			 * failed to step into a folder
			 * 
			 */

			warnx("can't read %s.\n", p->fts_path);
			break;
		case FTS_ERR:
		case FTS_NS:
			/*
			 * stat failure
			 * 
			 */

			warnc(p->fts_errno, "%s", p->fts_path);
			rval = 1;
			break;
		default:
			break;
		}
	}

	if (errno) {
		err(EPERM, "fts_read");
	}
	fts_close(fts);
	return rval;
}
